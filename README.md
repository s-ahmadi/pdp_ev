# READ ME #
This repository contains a model and the instances for Pickup-and-Delivery Problem (PDP) for Electric Vehicles (EVs).

## Resources

* Ahmadi, Saman, et al. "Vehicle Dynamics in Pickup-and-Delivery Problems Using Electric Vehicles", Proceedings of the CP Conference. 2021.

* Note: If you found our model and instances useful, please cite using the given references.
### What is this repository for? ###

* Testing and Running differnt energy models for PDP with EVs

### How do I get set up? ###

* Just look into the README file in project directories.
* Modelled in MiniZinc and tested with Chuffed Solver

### Who do I talk to? ###
* Email: Saman.ahmadi at rmit.edu.au
